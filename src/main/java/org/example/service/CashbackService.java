package org.example.service;

public class CashbackService {
    public int cashbackCalculate(int cash) {
        int result = cash / 100;
        int limit = 3000;
        if (result > limit) {
            return limit;
        }
        return result;
    }
}
