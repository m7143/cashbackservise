package org.example.service;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CashabackServiceTest {
  @Test
  void oneTransaction() {
    // (A)rrange
    final CashbackService service = new CashbackService();
    final int cash = 301;

    final int expected = 3;

    // (A)ct
    final int actual = service.cashbackCalculate(cash);

    // (A)ssert
    assertEquals(expected, actual);
  }

  @Test
  void limitCheck() {
    // (A)rrange
    final CashbackService service = new CashbackService();
    final int cash = 311100;
    final int expected = 3000;

    // (A)ct
    final int actual = service.cashbackCalculate(cash);

    // (A)ssert
    assertEquals(expected, actual);
  }






}
